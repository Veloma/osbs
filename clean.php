<?php
require("config.php");
//require("db.class.php");

$conn = new mysqli($dbserver, $dbuser, $dbpassword, $dbname);
// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "DROP DATABASE $dbname;";

if ($conn->query($sql) === TRUE) {
  $sql = "CREATE DATABASE $dbname;";
  if ($conn->query($sql) === TRUE) {
    echo "Purge réussie";
  }
  else
    echo "Erreur: " . $sql . "<br>" . $conn->error;
} else {
  echo "Erreur: " . $sql . "<br>" . $conn->error;
}

$conn->close();
?>
